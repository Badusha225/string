
public class RemoveDuplicatesFromSortedArray {

	public static void main(String[] args) {

		int[] arr= {2,5,3,8,5,6};
		System.out.println("Duplicate values in array:");
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<i; j++) {
				if(arr[i]==arr[j]) {
					System.out.println(arr[i]);
				}
			}
		}
		System.out.println("Sort Array:");
		for(int i=0; i<arr.length; i++) {
			for(int j=i+1; j<arr.length; j++) {
				if(arr[i] > arr[j]) {
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
			System.out.print(arr[i]);
		}
	}

}
